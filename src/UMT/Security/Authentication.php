<?php

namespace UMT\Security;

use \Config;
use Whoops\Exception\ErrorException;

class Authentication
{

	public function __construct()
	{

	}

	public static function login()
	{
		\phpCAS::client(SAML_VERSION_1_1, Config::get('security::cas_host'), Config::get('security::cas_port'), Config::get('security::cas_context'));
		\phpCAS::setNoCasServerValidation();
		\phpCAS::forceAuthentication();
		$user = \phpCAS::getUser();
		$userAttributes = (object)\phpCAS::getAttributes();

		\Session::put("user", $userAttributes);

		return $user;
	}

	public static function logout()
	{
		\phpCAS::client(SAML_VERSION_1_1, Config::get('security::cas_host'), Config::get('security::cas_port'), Config::get('security::cas_context'));
		\phpCAS::logout();

	}
}