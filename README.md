# UMT Security for CAS/Grouper

## Installation

    Add to your composer.json:
        "require": {
            "UMT/security": "dev-master"
        }

    Run 'php artisan config:publish umt/security' to publish local config

    Update app/config/app.php with:
        "providers" => array(
            ...
            "UMT\security\SecurityServiceProvider",
        ),

    edit app/packages/UMT/security/config.php to suit.

## Usage

    Security::require_valid_user();
    Security::require_by_group('GROUPER:GROUP:USERS');
    Security::require_valid_user_load_admin_group('GROUPER:GROUP:USERS');

### logout
#### routes.php
    Route::get('logout', function()
    {
        return UMT\Security\Security::logout();
    });